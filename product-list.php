<!DOCTYPE html>
<?php

require_once realpath("vendor/autoload.php");
?>

<html>
    <head>
	    <meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <title>Product List</title>
	    <link rel="stylesheet" type="text/css" href="Style/style.css">
	    <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Oswald&display=swap" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <script src="Script/product-list.js"></script>
    </head>
    <body>
	    <header class="header">
		    <div class="container">
			    <a href="#" class="product-page-logo"> Product Page
			    </a>
			    <div class="buttons-line">
			        <a href="add-product.php">
		                <button class="buttons" name="ADD">
			                ADD
				        </button>		
				    </a>		
			        <button id="delete-product-btn" class="buttons deleteProductBtn" name="MASS DELETE">
				        MASS DELETE
			        </button>
		        </div>
            </div>
        </header>
	    <div class="div-container">
	        <div class="div-line">		
	        </div>
        </div>
        <div class="container-product">
	        <?php
	            $ViewObject = new \ProductPage\MvcClasses\ProductsView();
	            $ViewObject->showProducts()
	        ?>
        </div>
	    <div class="div-container">
	        <div class="div-line">		
	        </div>
        </div>
        <footer class="div-container main-footer">
    	    <p> Scandiweb Test assignment</p>
        </footer>
    </body>
    <?php
        if(isset($_POST['id1']))
        {
	        $productcntrl = new \ProductPage\MvcClasses\ProductsContr(); 
            $productcntrl->delProduct($_POST['id1']);
        }
    ?>
</html>