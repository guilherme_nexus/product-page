<?php
require_once realpath("vendor/autoload.php");
?>
<?php
if(isset($_REQUEST['SubmitButton']))
{
    header("Location: product-list.php");
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
	    <meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <title>Product Add</title>
	    <link rel="stylesheet" type="text/css" href="Style/style.css">
	    <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Oswald&display=swap" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <script src="Script/add-product.js"></script> 
    </head>
    <body>
	    <header class="header">
		    <div class="container">
			    <a href="#" class="product-page-logo"> Product Page</a>
			    <button type="submit" form="product_form" class="buttons" value="submit" name="SubmitButton">
				Save				
			    </button>
			    <a href="index.php">
			    <button class="buttons">
				Cancel				
			    </button>
		        </a>
		    </div>
	    </header>
	    <div class="div-container">
	        <div class="div-line">		
	        </div>
        </div>
        <form id="product_form" class="form" method="POST" action="">
    	    <div class="form-container">
    	        <label> SKU
    		        <input type="text" name="sku" id="sku" placeholder="Stock Keeping Unit" required ></label>
    	        <label>Name
    		        <input type="text" name="name" id="name" placeholder="Product Name Here" required></label>
    	        <label>Price ($)
    		        <input type="number" step="0.01" name="price" id="price" placeholder="0.00" required>
    	        </label>
    	        <label>Type Switcher
    	            <select name="productType" id="productType" class="productType" required>
			            <option disabled selected value=""> -- Select a Type -- </option>
    		            <option value="DVD" id="DVD">DVD </option>
    		            <option value="Furniture" id="Furniture">Furniture </option>
    		            <option value="Book" id="Book">Book </option>
    	            </select>
                </label>
            </div>
		    <div id="detailsContainer" class="detailsContainer">
			</div>
        </form>
	    <div class="div-container">
	        <div class="div-line">		
	        </div>
        </div>
        <footer class="div-container main-footer">
    	    <p> Scandiweb Test assignment</p>
        </footer>
    </body>
    <?php
        if(isset($_REQUEST['SubmitButton']))
        {
            $objectCrtl = new \ProductPage\MvcClasses\ProductsContr();
            $objectCrtl->createProduct($_REQUEST);
        }
    ?>
</html>