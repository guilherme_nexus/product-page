
$(document).ready(function(){
    $('.productType').change(function(){
        $('.productType option:selected').each(function(){
        str = $(this).text();
        var myObject = eval("new " + str);
        myObject.showInputs();
        });
    });
});

class Product
{
    showInputs()
    {
    }
}

class DVD extends Product
{
    showInputs()
    {
        $('.detailsContainer').empty();
        $('.detailsContainer').append('<div class="details">',
        '<label>Size</label>',
        '<input type= "number" form="product_form" id="size" value="size" name="size" placeholder="e.g. 1024 MB" required>',
        '<p>Input the size of the Dvd in Megabytes (MB)</p></div>');
    }
}

class Book extends Product
{
    showInputs()
    {
        $('.detailsContainer').empty();
        $('.detailsContainer').append('<div class="details">',
        '<label>Weight</label>',
        '<input type= "number" id="weight" name="weight" value="weight" step="0.01" placeholder="e.g. 220 KG" required>', 
        '<p>Input the weight in Kilograms (KG)</p></div>');
    }
}

class Furniture extends Product
{
    showInputs()
    {
        $('.detailsContainer').empty();
        $('.detailsContainer').append('<div class="details">',
        '<label>Height</label>',
        '<input type= "number" id="height" step="0" name="height" value="height" placeholder="e.g. 20 CM" required>',
        '<label>Width (CM)</label>',
        '<input type="number" step = "0" name="width" value="width" id="width" placeholder="e.g. 20 CM" required>',  
        '<label>Length (CM)</label>',
        '<input type="number" step = "0" name="length" value="length" id="length" placeholder="e.g. 20 CM" required>',
        '<p>Please provide dimensions in HxWxL format</p></div>');
    }
}

