<?php

require_once realpath("vendor/autoload.php");

/**
 * Gets the id and uses it to delete that same id in the database
 */
if (isset($_POST['id1'])) {
	$productcntrl = new \ProductPage\MvcClasses\ProductsContr(); 
    $productcntrl->delProduct($_POST['id1']);
}
