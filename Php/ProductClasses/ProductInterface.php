<?php

namespace ProductPage\ProductClasses;

interface ProductInterface
{
    /**
     * Function to get object properties and put them into an array
     * @return array
     */
    public function getArray();

    /**
     *
     * shows the product in the product list
     * @return void
     */
    public function showProduct($id);

    /**
     *
     * Returns sql statement to use on setProduct function
     * @return string
     */
    public function getSql();

    /**
     *
     * Create instace of the object using an array of paramereters that came from the form
     * @return void
     */
    public function createInstance($parameters);
}
