<?php 

namespace ProductPage\ProductClasses;

require_once realpath("vendor/autoload.php");

class Furniture extends AbstractProduct implements ProductInterface
{
    /**
     * @var int
     */
    public $height;

    /**
     * @var int
     */
    public $width;

    /**
     * @var int
     */
    public $length; 

    /**
     *
     * @param int $height
     * @return void
     */ 
    public function setHeight($height)
    {
        $this->height = $height;
    }

    /**
     *
     * @param int $width
     * @return void
     */
    public function setWidth($width)
    {
        $this->width = $width;
    }

    /**
     *
     * @param int $length
     * @return void
     */
    public function setLength($length)
    {
        $this->length = $length;
    }

    /**
     * Create instace of the object using an array of paramereters that came from the form
     * @param array $parameters
     * @return void
     */
    public function createInstance($parameters)
    {
        $this->setSku($parameters['sku']);
        $this->setName($parameters['name']);
        $this->setPrice($parameters['price']);
        $this->setHeight($parameters['height']);
        $this->setWidth($parameters['width']);
        $this->setLength($parameters['length']);
    }

    /**
     * Returns sql statement to use on setProduct function
     * @return string
     */
    public function getSql()
    {
        $sql = "INSERT INTO products (sku, name, price, type, height, width, length) VALUES (:sku, :name, :price, :type, :height, :width, :length)";
        return $sql;
    }

    /**
     * returns object properties in form of an array
     * @return array
     */
    public function getArray()
    {
        $objectArray = array('sku'=> $this->sku, 'name' => $this->name, 'price' => $this->price, 'type' => 'Furniture', 'height' => $this->height, 'width' => $this->width, 'length' => $this->length);
        return $objectArray;
    }

    /**
     * 
     * @param int $id
     * @return void
     */
    public function showProduct($id)
    {
        echo "<div id={$id} class='product'><input value={$id} type='checkbox' class='delete-checkbox'><br>{$this->sku}<br>
        {$this->name}<br>$ {$this->price}<br>
        Dimensions: {$this->height}x{$this->width}x{$this->length}</div>"; 
    }
}