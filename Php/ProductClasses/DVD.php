<?php

namespace ProductPage\ProductClasses;

require_once realpath("vendor/autoload.php");

class DVD extends AbstractProduct implements ProductInterface
{
    /**
     *
     * @var int
     */
    public $size;

    /**
     *
     * @param int $size
     * @return void
     */
    public function setSize($size)
    
    {
        $this->size = $size;
    }

    /**
     * Create instace of the object using an array of paramereters that came from the form
     * @param array $parameters
     * @return void
     */
    public function createInstance($parameters)
    {
        $this->setSku($parameters["sku"]);
        $this->setName($parameters["name"]);
        $this->setPrice($parameters["price"]);
        $this->setSize($parameters["size"]);
    }

    /**
     * Returns sql statement to use on setProduct function
     * @return string
     */
    public function getSql()
    {
        $sql = "INSERT INTO products (sku, name, price, type, size) VALUES (:sku, :name, :price, :type, :size)";
        return $sql;
    }

    /**
     * returns object properties in form of an array
     * @return array
     */
    public function getArray()
    {
        $objectArray = array('sku'=> $this->sku, 'name' => $this->name, 'price' => $this->price, 'type' => 'DVD', 'size' => $this->size);
        return $objectArray;
    }

     /**
     *
     * @param int $id
     * @return void
     */
    public function showProduct($id)
    {
        echo "<div id={$id} class='product'><input value={$id} type='checkbox' class='delete-checkbox'><br>{$this->sku}<br>
            {$this->name}<br>$ {$this->price}<br>
            Size: {$this->size} MB</div>";
    }
}
