<?php 
namespace ProductPage\ProductClasses;
require_once realpath("vendor/autoload.php");

class Book extends AbstractProduct implements ProductInterface
{
    /**
     * @var float
     */
    public $weight;

    /**
     *
     * @param float $weight
     * @return void
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    /**
     * Create instace of the object using an array of paramereters that came from the form
     * @param array $parameters
     * @return void
     */
    public function createInstance($parameters)
    {
        $this->setSku($parameters['sku']);
        $this->setName($parameters['name']);
        $this->setPrice($parameters['price']);
        $this->setWeight($parameters['weight']);
    }

    /**
     * Returns sql statement to use on setProduct function
     * @return string
     */
    public function getSql()
    {
        $sql = "INSERT INTO products (sku, name, price, type, weight) VALUES (:sku, :name, :price, :type, :weight)";
        return $sql;
    }

    /**
     * returns object properties in form of an array
     * @return array
     */
    public function getArray()
    {
        $objectArray = array('sku'=> $this->sku, 'name' => $this->name, 'price' => $this->price, 'type' => 'Book', 'weight' => $this->weight);
        return $objectArray;
    }

     /**
     * shows the product in the product list
     * @param int $id
     * @return void
     */
    public function showProduct($id)
    {
        echo "<div id={$id} class='product'><input value={$id} type='checkbox' class='delete-checkbox'><br> {$this->sku}<br>
        {$this->name}<br>$ {$this->price}<br>
        Weight: {$this->weight} KG</div>";
    }
}
