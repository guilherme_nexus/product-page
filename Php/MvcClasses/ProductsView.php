<?php

namespace ProductPage\MvcClasses;

require_once realpath("vendor/autoload.php");

class ProductsView extends Products
{
    /**
     * Display the products
     *
     * @return void
     */
    public function showProducts()
    {
        $showProduct = new \ProductPage\MvcClasses\ShowProducts();
        $showProduct->showProducts();
    }
}
