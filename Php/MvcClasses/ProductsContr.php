<?php

namespace ProductPage\MvcClasses;

require_once realpath("vendor/autoload.php");

class ProductsContr extends Products
{
    /**
     * Creates the object and saves it in the database
     *
     * @param [type] $parameters
     * @return void
     */
    public function createProduct($parameters)
    {
        $createProduct = new \ProductPage\MvcClasses\CreateObject();
        $object = $createProduct->createObject($parameters);
        $this->setProduct($object);
    }

    /**
     * Deletes the product on the database based of its Id
     *
     * @param [type] $id
     * @return void
     */
    public function delProduct($id)
    {
        $this->deleteProduct($id);
    }
}
