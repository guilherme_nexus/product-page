<?php

namespace ProductPage\MvcClasses;

require_once realpath("vendor/autoload.php");

class Products extends Dbh
{
    /**
     * Get products form the database
     *
     * @return array
     */
    protected function getProducts()
    {
        $sql = "SELECT * FROM products";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute();
        $results = $stmt->fetchAll();
        return $results;
    }

    /**
     * Saves the product in the database
     *
     * @param object $object
     * @return void
     */
    protected function setProduct($object)
    {
        $sql = $object->getSql();
        $stmt = $this->connect()->prepare($sql);
        $array = $object->getArray();
        $stmt->execute($array);
    }

    /**
     * Deletes the product on the database based of its Id
     *
     * @param int $id
     * @return void
     */
    protected function deleteProduct($id)
    {
        $sql = "DELETE FROM products WHERE id=?";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute([$id]);
    }
}
