<?php

namespace ProductPage\MvcClasses;

require_once realpath("vendor/autoload.php");

class CreateObject
{
	/**
	 * 
	 * Creates an object and set its properties with the createInstance function
	 * @param array
	 * @return object
	 */
    public function createObject($parameters)
	{
		$parameters = array();
	    foreach ($_REQUEST as $key => $value) {
		    if (isset($value)) {
			    $parameters[$key] = $value; 
		    }
	    }
	    $type = $parameters['productType'];
	    $namespace = "\\ProductPage\\ProductClasses\\";
	    $str = $namespace . $type;
	    $object = new $str;
	    $object->createInstance($parameters);
	    return $object;
    }
}

