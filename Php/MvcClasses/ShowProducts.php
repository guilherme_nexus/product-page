<?php

namespace ProductPage\MvcClasses;

require_once realpath("vendor/autoload.php");

class ShowProducts extends Products 
{
    /**
     * Gets the products from the database, creates an object of the exact type
     * and displays it
     * @return void
     */
    public function showProducts()
    {
        $results = $this->getProducts();
        $j = count($results);
        for ($i= 0; $i< $j; $i++) {
            $type = $results[$i]['type'];
            $namespace = "\\ProductPage\\ProductClasses\\";
            $str = $namespace . $type;
            $object = new $str;
            $parameters = $results[$i];
            $object->createInstance($parameters);
            $object->showProduct($parameters["id"]);
        }
    }
}
